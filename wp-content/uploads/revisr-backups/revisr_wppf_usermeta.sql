
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wppf_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wppf_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wppf_usermeta` WRITE;
/*!40000 ALTER TABLE `wppf_usermeta` DISABLE KEYS */;
INSERT INTO `wppf_usermeta` VALUES (1,1,'nickname','p3l1cul45'),(2,1,'first_name',''),(3,1,'last_name',''),(4,1,'description',''),(5,1,'rich_editing','true'),(6,1,'comment_shortcuts','false'),(7,1,'admin_color','fresh'),(8,1,'use_ssl','0'),(9,1,'show_admin_bar_front','true'),(10,1,'wppf_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(11,1,'wppf_user_level','10'),(12,1,'dismissed_wp_pointers',''),(13,1,'default_password_nag',''),(14,1,'show_welcome_panel','1'),(26,1,'session_tokens','a:1:{s:64:\"ba8b47838ac49eeaea510068e77ca3d3a98cf70f92639e3fc311bea9bf615c45\";a:4:{s:10:\"expiration\";i:1476802134;s:2:\"ip\";s:13:\"186.89.20.112\";s:2:\"ua\";s:76:\"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0\";s:5:\"login\";i:1476629334;}}'),(16,1,'wppf_user-settings','editor=html&libraryContent=browse'),(17,1,'wppf_user-settings-time','1469422256'),(18,1,'wppf_dashboard_quick_press_last_post_id','3'),(19,1,'wppf_r_tru_u_x','a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1469426755;}'),(20,1,'wppf_wpmoly_dashboard_latest_movies_widget_settings','a:8:{s:15:\"movies_per_page\";i:8;s:9:\"show_year\";i:1;s:11:\"show_rating\";i:1;s:9:\"show_more\";i:1;s:10:\"show_modal\";i:1;s:14:\"show_quickedit\";i:1;s:13:\"style_posters\";i:1;s:13:\"style_metabox\";i:1;}'),(21,1,'wppf_wpmoly_dashboard_most_rated_movies_widget_settings','a:8:{s:15:\"movies_per_page\";i:4;s:9:\"show_year\";i:1;s:11:\"show_rating\";i:1;s:9:\"show_more\";i:1;s:10:\"show_modal\";i:1;s:14:\"show_quickedit\";i:1;s:13:\"style_posters\";i:1;s:13:\"style_metabox\";i:1;}'),(22,1,'tgmpa_dismissed_notice_tgmpa','1'),(27,1,'cuteslider_help_wp_pointer','1'),(23,1,'metaboxhidden_toplevel_page_wpmovielibrary','a:0:{}'),(24,1,'closedpostboxes_movie','a:0:{}'),(25,1,'metaboxhidden_movie','a:1:{i:0;s:7:\"slugdiv\";}');
/*!40000 ALTER TABLE `wppf_usermeta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

