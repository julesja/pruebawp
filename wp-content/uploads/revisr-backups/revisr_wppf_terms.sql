
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wppf_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wppf_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wppf_terms` WRITE;
/*!40000 ALTER TABLE `wppf_terms` DISABLE KEYS */;
INSERT INTO `wppf_terms` VALUES (1,'Sin categoría','sin-categoria',0),(2,'Uncategorized','uncategorized',0),(3,'Biz Cards','biz-cards',0),(4,'Branding','branding',0),(5,'Concept','concept',0),(6,'Creativity','creativity',0),(7,'Design','design',0),(8,'Ideas','ideas',0),(9,'Innovation','innovation',0),(10,'Motion','motion',0),(11,'Product','product',0),(12,'Photography Main Demo Menu','photography-main-demo-menu',0),(13,'Link','post-format-link',0),(14,'Video','post-format-video',0),(15,'Gallery','post-format-gallery',0),(16,'Quote','post-format-quote',0),(17,'Seth MacFarlane','seth-macfarlane',0),(18,'Comedia','comedia',0),(19,'Mark Wahlberg','mark-wahlberg',0),(20,'Seth MacFarlane','seth-macfarlane',0),(21,'Amanda Seyfried','amanda-seyfried',0),(22,'Jessica Barth','jessica-barth',0),(23,'Giovanni Ribisi','giovanni-ribisi',0),(24,'Morgan Freeman','morgan-freeman',0),(25,'Sam J. Jones','sam-j-jones',0),(26,'Patrick Warburton','patrick-warburton',0),(27,'Michael Dorn','michael-dorn',0),(28,'Bill Smitrovich','bill-smitrovich',0),(29,'John Slattery','john-slattery',0),(30,'Cocoa Brown','cocoa-brown',0),(31,'John Carroll Lynch','john-carroll-lynch',0),(32,'Ron Canada','ron-canada',0),(33,'Liam Neeson','liam-neeson',0),(34,'Dennis Haysbert','dennis-haysbert',0),(35,'Patrick Stewart','patrick-stewart',0),(36,'Tom Brady','tom-brady',0),(37,'Jay Leno','jay-leno',0),(38,'Jimmy Kimmel','jimmy-kimmel',0),(39,'Kate McKinnon','kate-mckinnon',0),(40,'Bobby Moynihan','bobby-moynihan',0),(41,'Taran Killam','taran-killam',0),(42,'Sebastian Arcelus','sebastian-arcelus',0),(43,'Jay Patterson','jay-patterson',0),(44,'Steve Callaghan','steve-callaghan',0),(45,'Nana Visitor','nana-visitor',0),(46,'Maggie Geha','maggie-geha',0),(47,'Jessica Szohr','jessica-szohr',0),(48,'Craig Ricci Shaynak','craig-ricci-shaynak',0),(49,'Lexi Atkins','lexi-atkins',0),(50,'Dustin Ybarra','dustin-ybarra',0),(51,'Julius Sharpe','julius-sharpe',0),(52,'Michael Steven Costello','michael-steven-costello',0),(53,'Barry J. Ratcliffe','barry-j-ratcliffe',0),(54,'Tina Grimm','tina-grimm',0),(55,'John Franchi','john-franchi',0),(56,'Claudia Zielke','claudia-zielke',0),(57,'Vincent M. Biscione','vincent-m-biscione',0),(58,'Alexandra Creteau','alexandra-creteau',0);
/*!40000 ALTER TABLE `wppf_terms` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

