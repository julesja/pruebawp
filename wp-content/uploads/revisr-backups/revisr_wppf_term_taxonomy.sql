
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wppf_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wppf_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wppf_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wppf_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wppf_term_taxonomy` VALUES (1,1,'category','',0,1),(2,2,'category','',0,8),(3,3,'portfolio-filter','',0,2),(4,4,'portfolio-filter','',0,5),(5,5,'portfolio-filter','',0,3),(6,6,'portfolio-filter','',0,6),(7,7,'portfolio-filter','',0,7),(8,8,'portfolio-filter','',0,2),(9,9,'portfolio-filter','',0,0),(10,10,'portfolio-filter','',0,0),(11,11,'portfolio-filter','',0,2),(12,12,'nav_menu','',0,6),(13,13,'post_format','',0,1),(14,14,'post_format','',0,1),(15,15,'post_format','',0,1),(16,16,'post_format','',0,2),(17,17,'collection','',0,1),(18,18,'genre','',0,1),(19,19,'actor','',0,1),(20,20,'actor','',0,1),(21,21,'actor','',0,1),(22,22,'actor','',0,1),(23,23,'actor','',0,1),(24,24,'actor','',0,1),(25,25,'actor','',0,1),(26,26,'actor','',0,1),(27,27,'actor','',0,1),(28,28,'actor','',0,1),(29,29,'actor','',0,1),(30,30,'actor','',0,1),(31,31,'actor','',0,1),(32,32,'actor','',0,1),(33,33,'actor','',0,1),(34,34,'actor','',0,1),(35,35,'actor','',0,1),(36,36,'actor','',0,1),(37,37,'actor','',0,1),(38,38,'actor','',0,1),(39,39,'actor','',0,1),(40,40,'actor','',0,1),(41,41,'actor','',0,1),(42,42,'actor','',0,1),(43,43,'actor','',0,1),(44,44,'actor','',0,1),(45,45,'actor','',0,1),(46,46,'actor','',0,1),(47,47,'actor','',0,1),(48,48,'actor','',0,1),(49,49,'actor','',0,1),(50,50,'actor','',0,1),(51,51,'actor','',0,1),(52,52,'actor','',0,1),(53,53,'actor','',0,1),(54,54,'actor','',0,1),(55,55,'actor','',0,1),(56,56,'actor','',0,1),(57,57,'actor','',0,1),(58,58,'actor','',0,1);
/*!40000 ALTER TABLE `wppf_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

